FROM openjdk:8-jre
RUN apt-get update && apt-get install -t jessie-backports --yes r-base
COPY libjri.so /opt/lib/libjri.so
