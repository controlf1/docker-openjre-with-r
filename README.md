# OpenJRE with R

This repository contains the source code for a Docker image which allows interfacing from JVM based languages to [R](https://www.r-project.org/), the data science / statistics language.

The image itself is available on [Docker Hub](https://hub.docker.com/r/controlf1/openjre-with-r/).

`libjri.so` in this repository is extracted from the [JRI Maven artifact](https://mvnrepository.com/artifact/org.nuiton.thirdparty/JRI/0.9-6).